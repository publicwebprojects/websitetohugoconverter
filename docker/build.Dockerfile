FROM golang-base-1.13:latest AS golang

WORKDIR /work/src/gitlab.com/bartek1912/websitetohugoconverter
COPY Gopkg.toml Gopkg.lock  ./
RUN dep ensure --vendor-only

COPY . /work/src/gitlab.com/bartek1912/websitetohugoconverter

RUN make install-deps
ARG RUNPACKAGE
RUN go test -race ./${RUNPACKAGE}/...
RUN go build -v -a -o /work/app/converter -tags netgo ./${RUNPACKAGE}

FROM alpine:latest
WORKDIR /app
COPY --from=golang /work/app/converter converter

RUN apk update &&\
    apk add --no-cache ca-certificates

CMD [ "/app/converter" ]
