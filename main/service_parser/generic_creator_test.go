package parser_test

import (
	"github.com/stretchr/testify/assert"
	parser "gitlab.com/bartek1912/websitetohugoconverter/main/service_parser"
	"testing"
)

func TestExtractGeneric_ZeroDocuments(t *testing.T) {
	//Given
	input := []parser.HTMLEnrichedRows{}

	//When
	pref, suf := parser.ExtractGenericPrefixAndSufix(input)

	//Then
	assert.Len(t, pref, 0)
	assert.Len(t, suf, 0)
}
func TestExtractGeneric_TwoExactDocuments(t *testing.T) {
	//Given
	s1 := parser.HTMLRow{Path: ">html>head>title", Text: "XXX"}
	s2 := parser.HTMLRow{Path: ">html>body", Text: "Hello"}
	document := parser.HTMLEnrichedRows{
		Rows: []parser.HTMLRow{
			s1,
			s2,
		},
	}
	input := []parser.HTMLEnrichedRows{document, document}

	//When
	pref, suf := parser.ExtractGenericPrefixAndSufix(input)

	//Then
	assert.Equal(t, document.Rows, pref)
	assert.Len(t, suf, 0)
}

func TestExtractGeneric_TwoDocumentsSamePrefix(t *testing.T) {
	//Given
	prefStripped := ">html>head>title"
	pref := parser.HTMLRow{prefStripped, "XXX"}
	document1 := parser.HTMLEnrichedRows{
		Rows: []parser.HTMLRow{
			pref,
			{">html>body", "Hello"},
		},
	}
	document2 := parser.HTMLEnrichedRows{
		Rows: []parser.HTMLRow{
			pref,
			{">html>body>div", "Hello"},
		},
	}
	input := []parser.HTMLEnrichedRows{document1, document2}
	answer := []parser.HTMLRow{pref}

	//When
	prefRes, sufRes := parser.ExtractGenericPrefixAndSufix(input)

	//Then
	assert.Len(t, sufRes, 0)
	assert.Equal(t, answer, prefRes)
}

func TestExtractGeneric_TwoDocumentsSameSufix(t *testing.T) {
	//Given
	sufPath := ">html>div>footer"
	sufText := "XXX"
	suf := parser.HTMLRow{Path: sufPath, Text: sufText}
	document1 := parser.HTMLEnrichedRows{
		Rows: []parser.HTMLRow{
			{Path: ">html>body", Text: sufText},
			suf,
		},
	}
	document2 := parser.HTMLEnrichedRows{
		Rows: []parser.HTMLRow{
			{Path: ">html>body>div", Text: sufText},
			suf,
		},
	}
	input := []parser.HTMLEnrichedRows{document1, document2}
	ans := []parser.HTMLRow{suf}

	//When
	prefRes, sufRes := parser.ExtractGenericPrefixAndSufix(input)

	//Then
	assert.Len(t, prefRes, 0)
	assert.Equal(t, ans, sufRes)
}

func TestExtractGeneric_TwoDocumentWithDifferentTitle(t *testing.T) {
	//Given
	sufPath := ">html>body>div"
	suf := parser.HTMLRow{sufPath, "XYZ"}
	document1 := parser.HTMLEnrichedRows{
		Rows: []parser.HTMLRow{
			{">html>head>title", "T1"},
			{">html>body", "Hello"},
			suf,
		},
	}
	document2 := parser.HTMLEnrichedRows{
		Rows: []parser.HTMLRow{
			{">html>head>title", "T2"},
			{">html>body>div", "Hello"},
			suf,
		},
	}
	answerPref := []parser.HTMLRow{{Path: ">html>head>title"}}
	answerSuf := []parser.HTMLRow{suf}
	input := []parser.HTMLEnrichedRows{document1, document2}

	//When
	prefRes, sufRes := parser.ExtractGenericPrefixAndSufix(input)

	//Then
	assert.Equal(t, answerPref, prefRes)
	assert.Equal(t, answerSuf, sufRes)
}
