package parser_test

import (
	"github.com/stretchr/testify/assert"
	parser "gitlab.com/bartek1912/websitetohugoconverter/main/service_parser"
	"testing"
)

//TODO refactor tests as all of them have same structure - maybe fixture??
func TestGenerateDump_EmptyStructure(t *testing.T) {
	//Given
	text := "<html><head></head><body></body></html>"

	//When
	res, err := parser.GenerateDump(text)

	//Then
	assert.Nil(t, err)
	assert.Len(t, res.Rows, 0)
}

func TestGenerateDump_MultilineText(t *testing.T) {
	//Given
	text := `<html><body><p>XXXX

		Y</p></body></html>`
	ans := []parser.HTMLRow{
		{Path: `>html>body>p`, Text: `XXXX

		Y`},
	}
	//When
	res, err := parser.GenerateDump(text)

	//Then
	assert.Nil(t, err)
	assert.Equal(t, ans, res.Rows)
}
func TestGenerateDump_SimpleStructure(t *testing.T) {
	//Given
	text := "<html><head><title>T</title></head><body><p>XXXX</p></body></html>"
	ans := []parser.HTMLRow{
		{">html>head>title", "T"},
		{">html>body>p", "XXXX"},
	}
	//When
	res, err := parser.GenerateDump(text)

	//Then
	assert.Nil(t, err)
	assert.Equal(t, ans, res.Rows)
}

func TestGenerateDump_NestedMixedStructure(t *testing.T) {
	//Given
	text := "<html><body>AAAA<p>XXXX<div>BBBB</div>YY<p>CC</p>D</p>EEE</body>F</html>"
	ans := []parser.HTMLRow{
		{Path: ">html>body", Text: "AAAA"},
		{Path: ">html>body>p", Text: "XXXX"},
		{Path: ">html>body>p>div", Text: "BBBB"},
		{Path: ">html>body>p", Text: "YY"},
		{Path: ">html>body>p>p", Text: "CC"},
		{Path: ">html>body>p", Text: "D"},
		{Path: ">html>body", Text: "EEE"},
		{Path: ">html", Text: "F"},
	}
	//When
	res, err := parser.GenerateDump(text)

	//Then
	assert.Nil(t, err)
	assert.Equal(t, ans, res.Rows)
}

func TestGenerateDump_TagStartedButNotEnded(t *testing.T) {
	//Given
	text := "<html><body><p>XXXX<div><div>YY</p>ZZ<div>AA</div></body></html>"
	ans := []parser.HTMLRow{
		{Path: ">html>body>p", Text: "XXXX"},
		{Path: ">html>body>p>div>div", Text: "YY"},
		{Path: ">html>body", Text: "ZZ"},
		{Path: ">html>body>div", Text: "AA"},
	}
	//When
	res, err := parser.GenerateDump(text)

	//Then
	assert.Nil(t, err)
	assert.Equal(t, ans, res.Rows)
}
func TestGenerateDump_EndOfNonExistingTag(t *testing.T) {
	//Given
	text := "<html><body><p>XXXX<div>YY</img>ZZ</p>AA</body></html>"
	ans := []parser.HTMLRow{
		{Path: ">html>body>p", Text: "XXXX"},
		{Path: ">html>body>p>div", Text: "YYZZ"},
		{Path: ">html>body", Text: "AA"},
	}
	//When
	res, err := parser.GenerateDump(text)

	//Then
	assert.Nil(t, err)
	assert.Equal(t, ans, res.Rows)
}

func TestGenerateDump_SelfClosingTag(t *testing.T) {
	//Given
	text := "<html><body><br/><p>XXXX<br/></p><br/></body></html>"
	ans := []parser.HTMLRow{
		{Path: ">html>body", Text: "<br/>"},
		{Path: ">html>body>p", Text: "XXXX<br/>"},
		{Path: ">html>body", Text: "<br/>"},
	}
	//When
	res, err := parser.GenerateDump(text)

	//Then
	assert.Nil(t, err)
	assert.Equal(t, ans, res.Rows)
}

func TestGenerateDump_TwoDifferentTablesButSameOutput(t *testing.T) {
	//Given
	text := "<table><tr><td>AA</td></tr><tr><td>BB</td></tr></table>"
	text2 := "<table><tr><td>AA</td><td>BB</td></tr></table>"
	ans := []parser.HTMLRow{
		{Path: ">table>tr>td", Text: "AA"},
		{Path: ">table>tr>td", Text: "BB"},
	}
	//When
	res, err := parser.GenerateDump(text)
	res2, err2 := parser.GenerateDump(text2)

	//Then
	assert.Nil(t, err)
	assert.Nil(t, err2)
	assert.Equal(t, ans, res.Rows)
	assert.Equal(t, ans, res2.Rows)
	assert.Equal(t, res2.Rows, res.Rows) //Kind of expected
}
