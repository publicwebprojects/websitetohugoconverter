package parser

import (
	"fmt"
	"github.com/rs/zerolog/log"
)

type WebPage struct {
	Title text `xml:"head>title"`
	Body  text `xml:"body"`
}
type text struct {
	Text string `xml:",innerxml"`
}

type WebDirectory struct {
	Generic string
	Content map[string]WebPage
	//static, images etc.
}

type WebServiceTemplate struct {
	Url  string
	Root WebDirectory
}

type Parser interface {
	ToWebServiceTemplate(map[string]string) (WebServiceTemplate, error)
}

type parser struct {
}

func (p *parser) ToWebServiceTemplate(str map[string]string) (WebServiceTemplate, error) {
	var res WebServiceTemplate
	res.Root.Content = make(map[string]WebPage)
	//mn := 1e15
	//for _, cont := range str {
	//	mn := math.Min(float64(len(cont)), float64(mn))
	//}
	//for max_pref := 0; max_pref < mn; max_pref ++ {
	//	for url, cont := range str {
	//		res.Url = url
	//		page := WebPage{}
	//		err := xml.NewDecoder(strings.NewReader(cont)).Decode(&page)
	//		if err != nil {
	//			log.Warn().Err(err).Msgf("got an error during parsing %v", url)
	//		} else {
	//			res.Root.Content[url] = page
	//		}
	//	}
	//}
	ind := 0
	dumps := make([]HTMLEnrichedRows, len(str))
	for url, cont := range str {
		var err error
		dumps[ind], err = GenerateDump(cont)
		if err != nil {
			log.Warn().Err(err).Msgf(
				"Got an error during creating dump of %v", url)
		}
		ind++
	}
	ExtractGenericPrefixAndSufix(dumps)
	res.Root.Generic = "" //TODO return kind of generic with parameters structure
	if len(res.Root.Content) < 2 {
		return WebServiceTemplate{}, fmt.Errorf("parsed only %v pages - requires at least 2", len(res.Root.Content))
	}
	return res, nil
}

func MakeNewParser() Parser {
	return &parser{}
}
