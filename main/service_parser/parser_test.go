package parser_test

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	parser "gitlab.com/bartek1912/websitetohugoconverter/main/service_parser"
	"testing"
)

func TestParser_ToWebServiceTemplateNoGivenPage(t *testing.T) {
	//Given
	p := parser.MakeNewParser()
	pages := map[string]string{}

	//When
	_, err := p.ToWebServiceTemplate(pages)

	//Then
	assert.NotNil(t, err)
}

func TestParser_ToWebServiceTemplateSinglePageReturnsError(t *testing.T) {
	//Given
	p := parser.MakeNewParser()
	title := "something"
	content := "<div>xyz</div>"
	url := "http://somepage.com"
	header := fmt.Sprintf(`<html>
		<head>
			<title>%v</title>
		</head>
		<body>`, title)
	footer := `</body>
	</html>`
	page := fmt.Sprintf(
		`%v %v %v`, header, content, footer)
	pages := make(map[string]string)
	pages[url] = page

	//When
	_, err := p.ToWebServiceTemplate(pages)

	//Then
	assert.NotNil(t, err)
}
func TestParser_ToWebServiceTemplateThreePages(t *testing.T) {
	t.Skip()
	//Given
	p := parser.MakeNewParser()
	title := "something"
	content := "<div>xyz</div>"
	content2 := "<p>a</p>"
	url := "http://somepage.com"
	url2 := url + "/subpage"
	header := fmt.Sprintf(`<html>
		<head>
			<title>%v</title>
		</head>
		<body>`, title)
	footer := `</body>
	</html>`
	page := fmt.Sprintf(
		`%v %v %v`, header, content, footer)
	page2 := fmt.Sprintf(
		`%v %v %v`, header, content2, footer)
	pages := make(map[string]string)
	pages[url] = page
	pages[url2] = page2

	//When
	r, err := p.ToWebServiceTemplate(pages)

	//Then
	assert.Nil(t, err)
	assert.Equal(t, url, r.Url)
	assert.Len(t, r.Root.Content, 1)
	assert.Equal(t, title, r.Root.Content[url].Title.Text)
	assert.Equal(t, content, r.Root.Content[url].Body.Text)
	//assert.Equal(t, header, r.Root.Header)
	//assert.Equal(t, footer, r.Root.Header)
}
