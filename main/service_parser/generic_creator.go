package parser

type Template struct {
	Prefix     HTMLRow
	Suffix     HTMLRow
	Parameters []string
}

//TODO extract this method into one
func ExtractGenericPrefixAndSufix(documents []HTMLEnrichedRows) ([]HTMLRow, []HTMLRow) {
	if len(documents) == 0 {
		return make([]HTMLRow, 0), make([]HTMLRow, 0)
	}
	//TODO we may want to choose the shortest one - will improve performance
	firstDocument := documents[0]
	var prefRows []HTMLRow
prefixLoop:
	for rowInd, row := range firstDocument.Rows {
		sameText := true
		for _, document := range documents {
			if document.Rows[rowInd].Path != row.Path {
				break prefixLoop
			}
			sameText = sameText && document.Rows[rowInd].Text == row.Text
		}
		if sameText {
			prefRows = append(prefRows, row)
		} else {
			prefRows = append(prefRows, HTMLRow{Path: row.Path})
		}
	}
	var sufRows []HTMLRow
	if len(prefRows) != len(firstDocument.Rows) {
	suffixLoop:
		for rowInd := len(firstDocument.Rows) - 1; rowInd >= 0; rowInd-- {
			row := firstDocument.Rows[rowInd]
			sameText := true
			for _, document := range documents {
				if document.Rows[rowInd].Path != row.Path {
					break suffixLoop
				}
				sameText = sameText && document.Rows[rowInd].Text == row.Text
			}
			if sameText {
				sufRows = append(sufRows, row)
			} else {
				sufRows = append(sufRows, HTMLRow{Path: row.Path})
			}
		}
		//Prevent intelij linter false-positive warning
		if len(sufRows) > 0 {
			for ind := 0; ind < len(prefRows)/2; ind++ {
				indRev := len(prefRows) - ind - 1
				sufRows[ind], sufRows[indRev] = sufRows[indRev], sufRows[ind]
			}
		}
	}
	return prefRows, sufRows
}

//
//func extractCommonPart(pattern HTMLEnrichedRows, it HTMLRowIterator) []HTMLRow {
//	var prefRows []HTMLRow
//	for rowInd, row := range pattern.Rows {
//		sameText := true
//		for _, document := range documents {
//			if document.Rows[rowInd].Path != row.Path {
//				return prefRows
//			}
//			sameText = sameText && document.Rows[rowInd].Text == row.Text
//		}
//		if sameText {
//			prefRows = append(prefRows, row)
//		} else {
//			prefRows = append(prefRows, HTMLRow{Path: row.Path})
//		}
//	}
//	return prefRowsl
//}
