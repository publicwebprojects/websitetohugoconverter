package parser

//TODO this needs to be rewrite when it will be used
import (
	"fmt"
	"golang.org/x/net/html"
	"strings"
)

type HTMLRow struct {
	Path string
	Text string
}

type HTMLEnrichedRows struct {
	Rows []HTMLRow
}

func GenerateDump(str string) (HTMLEnrichedRows, error) {
	var res []HTMLRow
	z := html.NewTokenizer(strings.NewReader(str))
	var path []string    //TODO maybe move this to slice of byte slices to reduce the number of conversions
	pathChanged := false //TODO this may be replaced with simple check of last pushed element - less error prone
	//TODO reduce number of to string conversions
mainLoop:
	for {
		tt := z.Next()
		switch tt {
		//TODO DoctypeToken
		case html.ErrorToken:
			if z.Err().Error() != "EOF" {
				return HTMLEnrichedRows{}, z.Err()
			}
			break mainLoop
		case html.TextToken:
			if !pathChanged {
				res[len(res)-1].Text += string(z.Text())
			} else {
				res = append(res, generatePathStr(path, z.Text()))
			}
			pathChanged = false
		case html.SelfClosingTagToken:
			t, _ := z.TagName()
			tagName := "<" + string(t) + "/>"
			fmt.Println("Got tag: " + tagName)
			if !pathChanged {
				res[len(res)-1].Text += tagName
			} else {
				res = append(res, generatePathStr(path, []byte(tagName)))
			}
			pathChanged = false
		case html.StartTagToken:
			tagName, _ := z.TagName()
			path = append(path, string(tagName))
			pathChanged = true
		case html.EndTagToken:
			if len(path) == 0 {
				break mainLoop
			}
			tagName, _ := z.TagName()
			index := len(path) - 1
			for ; index >= 0; index -= 1 {
				//TODO this conversion shouldn't be done in loop
				if path[index] == string(tagName) {
					break
				}
			}
			if index == -1 {
				continue
			}
			path = path[:index]
			pathChanged = true
		}
	}
	return HTMLEnrichedRows{Rows: res}, nil
}

func generatePathStr(path []string, text []byte) HTMLRow {
	prefix := ""
	for _, v := range path {
		prefix += ">" + v
	}
	return HTMLRow{Path: prefix, Text: string(text)}
}
