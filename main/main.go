package main

import (
	"fmt"
	crawler2 "gitlab.com/bartek1912/websitetohugoconverter/main/crawler"
	"gitlab.com/bartek1912/websitetohugoconverter/main/exporter"
	"net/url"
	"os"
)

func main() {
	if len(os.Args) != 3 {
		fmt.Println("Required two arguments: url output_dir ")
	}
	addressRaw, _ := os.Args[1], os.Args[2]
	address, err := url.Parse(addressRaw)
	if err != nil {
		fmt.Printf("Couldn't parse url: '%v'", addressRaw)
		os.Exit(1)
	}
	crawler := crawler2.MakeNewCrawler()
	pageStructure, err := crawler.GetPageStructure(*address, 0)
	if err != nil {
		fmt.Println("Got an error ", err)
		return
	}
	exp := exporter.NewFileExporter()
	err = exp.Export(pageStructure)
	if err != nil {
		fmt.Println("Got an error ", err)
		return
	}
	fmt.Println("page structure", pageStructure)
}
