package crawler_test

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	crawler2 "gitlab.com/bartek1912/websitetohugoconverter/main/crawler"
	"net/url"
	"testing"
)

func TestCrawler_GetPageStructureWhenSinglePageWebsiteIsValid(t *testing.T) {
	//Given
	crawler := crawler2.MakeNewCrawlerWithPageGetter(func(a url.URL) (string, error) {
		return a.String(), nil
	})
	url, err := url.Parse("X")
	if err != nil {
		t.Fail()
	}

	//When
	res, err := crawler.GetPageStructure(*url, 0)

	//Then
	assert.Nil(t, err)
	assert.Len(t, res, 1)
}

func TestCrawler_GetPageStructureWhenSinglePageWebsiteReturnsError(t *testing.T) {
	//Given
	crawler := crawler2.MakeNewCrawlerWithPageGetter(func(url.URL) (string, error) {
		return "", fmt.Errorf("Expected error ")
	})
	url, err := url.Parse("X")
	if err != nil {
		t.Fail()
	}

	//When
	res, err := crawler.GetPageStructure(*url, 0)

	//Then
	assert.NotNil(t, err)
	assert.Len(t, res, 0)
}
