package crawler

import (
	"io/ioutil"
	"net/http"
	"net/url"
	"time"
)

type Crawler interface {
	GetPageStructure(url url.URL, interval time.Duration) (map[url.URL]string, error)
}

type crawler struct {
	getPage func(url.URL) (string, error)
}

func (c crawler) GetPageStructure(address url.URL, interval time.Duration) (map[url.URL]string, error) {
	r := make(map[url.URL]string)
	content, err := c.getPage(address)
	if err != nil {
		return r, err
	}
	r[address] = content
	return r, nil
}

//func (c crawler) Crawl(url string){
//	new_links := make(chan string, 5)
//	linksToFetch := make(chan string)
//	go c.watchNewLinks(new_links, linksToFetch)
//	go c.fetchNewLinks(linksToFetch, new_links)
//}

//func (c crawler) watchNewLinks(new_links chan string, out chan string) {
//	crawled := make(map[string]bool)
//	for u := range new_links {
//		_, ok := crawled[u]
//		if ok {
//			crawled[u] = true
//			out <- u
//		}
//	}
//	close(new_links)
//}
//func (c crawler) fetchNewLinks(linksToFetch chan string, proposedUrls chan string) {
//	for url := range linksToFetch {
//
//	}
//}
func MakeNewCrawler() Crawler {
	getPage := func(address url.URL) (string, error) {
		resp, err := http.Get(address.String())
		if err != nil {
			return "", err
		}
		defer resp.Body.Close()
		html, err := ioutil.ReadAll(resp.Body)
		return string(html), err
	}
	return MakeNewCrawlerWithPageGetter(getPage)
}

func MakeNewCrawlerWithPageGetter(getPage func(url.URL) (string, error)) Crawler {
	return &crawler{getPage: getPage}
}
