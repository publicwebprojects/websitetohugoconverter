package exporter_test

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/bartek1912/websitetohugoconverter/main/exporter"
	"gitlab.com/bartek1912/websitetohugoconverter/main/exporter/mocks"
	"net/url"
	"testing"
)

func TestFile_Export_EmptyMap(t *testing.T) {
	//Given
	write := &mocks.FileWriterInterface{}
	defer write.AssertExpectations(t)
	f := exporter.NewFileExporterWithWriter(write.WriteFile)
	var input map[url.URL]string

	//When
	err := f.Export(input)

	//Then
	assert.NotNil(t, err)
}

func TestFile_Export_EncodeUrlToPath(t *testing.T) {
	//Given
	testTable := []struct {
		url  string
		path string
	}{
		{"http://abcd.pl/", "abcd.pl/index.html"},
		{"https://sub.sub2.onet.eu", "sub.sub2.onet.eu/index.html"},
	}

	write := &mocks.FileWriterInterface{}
	defer write.AssertExpectations(t)
	f := exporter.NewFileExporterWithWriter(write.WriteFile)
	input := make(map[url.URL]string)
	for _, row := range testTable {
		parsed, err := url.Parse(row.url)
		if err != nil || parsed == nil {
			t.Fail()
		}
		input[*parsed] = "XXX"
		write.On("WriteFile", row.path, mock.Anything, mock.Anything).Return(nil)
	}

	//When
	err := f.Export(input)

	//Then
	assert.Nil(t, err)
}

func TestFile_Export_ErrorWhileSaving(t *testing.T) {
	//Given
	u1, _ := url.Parse("abcd.pl.html")
	input := map[url.URL]string{*u1: "adsa"}
	write := &mocks.FileWriterInterface{}
	defer write.AssertExpectations(t)
	f := exporter.NewFileExporterWithWriter(write.WriteFile)

	//When
	write.On("WriteFile", mock.Anything, mock.Anything, mock.Anything).Return(fmt.Errorf("expected error"))
	err := f.Export(input)

	//Then
	assert.NotNil(t, err)
}
