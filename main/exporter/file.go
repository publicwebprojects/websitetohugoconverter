package exporter

import (
	"fmt"
	"io/ioutil"
	"net/url"
	"os"
	"strings"
)

type Exporter interface {
	Export(map[url.URL]string) error
}

type FileWriter func(filename string, data []byte, perm os.FileMode) error

//Interface created only for mockery - it generates mock based on that interface
type FileWriterInterface interface {
	WriteFile(filename string, data []byte, perm os.FileMode) error
}

type file struct {
	writer FileWriter
}

func (f file) Export(m map[url.URL]string) error {
	if len(m) == 0 {
		return fmt.Errorf("given empty map to export")
	}
	const suffix = ".html"
	for k, v := range m {
		path := k.Host
		if len(k.Path) <= 1 {
			path += "/index.html"
		}
		if !strings.HasSuffix(path, suffix) {
			path += suffix
		}
		err := f.writer(path, []byte(v), 777)
		if err != nil {
			return err
		}
	}
	return nil
}

func NewFileExporter() Exporter {
	return NewFileExporterWithWriter(ioutil.WriteFile)
}

func NewFileExporterWithWriter(write FileWriter) Exporter {
	return &file{writer: write}
}
