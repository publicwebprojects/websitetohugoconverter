# WebsiteToHugoConverter

The project target it to create automate tool that can convert any page to static hugo template. 

What it can do: it can download single page and save it in the same directory.

# Next step

Let it download and copy all the pages from given url reproducing the directory tree.

# Bug

* Don't know how to parse tables. They might be flatten. Need to add start/end tag event,

# Future works

* Choose the output directory via cmd argument
* Change crawler dependency injection to the nice way used in file
* generate mocks using make install-deps and remove them from git
* Add CI/CD
* Architecture/scaling concept: https://www.draw.io/#G1u2BZb48AdJBGjLjV_pvG-1lYNceUrp-a