.PHONY: test build
export RUNPACKAGE=main
export IMAGE=converter
test:
	@ docker-compose -f docker/docker-compose.yaml up --exit-code-from converter
build:
	@docker build \
		--label "git_commit=$(GIT_COMMIT)" \
		-f docker/build.Dockerfile \
		-t $(IMAGE) \
		--build-arg RUNPACKAGE=$(RUNPACKAGE) \
		.
install-deps:
	dep ensure -v